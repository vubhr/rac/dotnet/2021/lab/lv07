using System.Collections;

namespace InternetShop.Models;

public interface IProductService {
  Task<IEnumerable> All();
  Task<IEnumerable> AvailableByQuantity();
  Task<IEnumerable> AvailableByPrice();
  Task<IEnumerable> OnSale();
  Task<Product?> Get(int? id);
  Task<int> Insert(Product? product);
  Task<bool> Update(Product? product);
  Task<int> Delete(int id);
  InternetShopDbContext Db { get; }
}