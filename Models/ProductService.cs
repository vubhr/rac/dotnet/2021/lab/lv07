using System.Collections;
using Microsoft.EntityFrameworkCore;

namespace InternetShop.Models;

public class ProductService : IProductService {
  public ProductService(InternetShopDbContext context) {
    Db = context;
  }

  public async Task<IEnumerable> All() {
    return await Db.Products.ToListAsync();
  }

  public async Task<IEnumerable> AvailableByQuantity() {
    return await Db.Products
      .Where(p => p.Quantity > 0)
      .OrderByDescending(p => p.Quantity)
      .ToListAsync();
  }

  public async Task<IEnumerable> AvailableByPrice() {
    return await Db.Products
      .Where(p => p.Quantity > 0)
      .OrderBy(p => p.Price)
      .ToListAsync();
  }

  public async Task<IEnumerable> OnSale() {
    return await Db.Products
      .Where(p => p.OnSale == true)
      .ToListAsync();
  }

  public async Task<Product?> Get(int? id) {
    return await Db.Products.FirstOrDefaultAsync(m => m.Id == id);
  }

  public async Task<int> Insert(Product? product) {
    if (product != null) {
        Db.Add(product);
        return await Db.SaveChangesAsync();
    }
    return -1;
  }

  public async Task<bool> Update(Product? product) {
    if (product != null) {
      try {
        Db.Update(product);
        await Db.SaveChangesAsync();
        return true;
      } catch (DbUpdateConcurrencyException) {
        return false;
      }
    }
    return false;
  }

  public async Task<int> Delete(int id) {
      var product = await Get(id);
      Db.Products.Remove(product);
      return await Db.SaveChangesAsync();
  }

  public InternetShopDbContext Db { get; }
}