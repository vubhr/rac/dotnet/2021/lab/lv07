namespace InternetShop.Models;

public class Product {
  public int Id { get; set; }
  public string? Title { get; set; }
  public string? Description { get; set; }
  public decimal Price { get; set; }
  public int Quantity { get; set; }
  public bool OnSale { get; set; }
  public int SalePercentage {
      get {
          if (Price > 0) {
            return (int)(Math.Round(((Price - SalePrice) / Price) * 100, 0));
          }
          return 0;
      }
  }

  public decimal SalePrice { get; set; }
}
