using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using InternetShop.Models;

namespace InternetShop.Controllers {
  public class ProductsController : Controller {
    public ProductsController(IProductService ps) {
      this.ps = ps;
    }

    public async Task<IActionResult> Index() {
      return View(await ps.All());
    }

    public async Task<IActionResult> AvailableByQuantity() {
      return View(await ps.AvailableByQuantity());
    }

    public async Task<IActionResult> AvailableByPrice() {
      return View(await ps.AvailableByPrice());
    }
    public async Task<IActionResult> Sale() {
      return View(await ps.OnSale());
    }

    public async Task<IActionResult> Details(int? id) {
      if (id == null) return NotFound();

      Product? product;
      if ((product = await ps.Get(id)) != null) {
        return View(product);
      } else {
        return NotFound();
      }
    }

    public IActionResult Create() {
      return View();
    }

    [HttpPost]
    [ValidateAntiForgeryToken]
    public async Task<IActionResult> Create([Bind("Id,Title,Description,Price,Quantity,OnSale,SalePrice")] Product product) {
      if (ModelState.IsValid) {
        await ps.Insert(product);
        return RedirectToAction(nameof(Index));
      }
      return View(product);
    }

    public async Task<IActionResult> Edit(int? id) {
      if (id == null) return NotFound();

      Product? product;
      if ((product = await ps.Get(id)) != null) {
        return View(product);
      } else {
        return NotFound();
      }
    }

    [HttpPost]
    [ValidateAntiForgeryToken]
    public async Task<IActionResult> Edit(int id, [Bind("Id,Title,Description,Price,Quantity,OnSale,SalePrice")] Product product) {
      if (id != product.Id) return NotFound();

      if (ModelState.IsValid) {
        if (await ps.Update(product) == true) {
          return RedirectToAction(nameof(Index));
        }
      }
      return View(product);
    }

    public async Task<IActionResult> Delete(int? id) {
      if (id == null) return NotFound();

      Product? product;
      if ((product = await ps.Get(id)) != null) {
        return View(product);
      } else {
        return NotFound();
      }
    }

    [HttpPost, ActionName("Delete")]
    [ValidateAntiForgeryToken]
    public async Task<IActionResult> DeleteConfirmed(int id) {
      await ps.Delete(id);
      return RedirectToAction(nameof(Index));
    }

    private readonly IProductService ps;
  }
}
